const express = require('express')
const mongodb = require('mongodb')
const router = require('./router')
const app = express()
const PORT = 7500
/* let db */
let connectionString = `mongodb://localhost:27017/todos`
mongodb.connect(
  connectionString,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  },
  function (err, client) {
    global.db = client.db()
  }
)
app.use(express.json())

app.use('/todos',router)

app.listen(PORT, () => {
  console.log(`Node.js App running on port ${PORT}...`)
})

module.exports = app;