
const express = require('express');
const router = express.Router();
const mongodb = require('mongodb');

// create a data
router.post('/create-todo', function (req, res) {
  db.collection('data').insertOne({ name: req.body.name, done: req.body.done },
    function (
      err,
      info
    ) {
      res.json(info.ops[0])
    })
})

// getting all the data
router.get('/', function (req, res) {
  db.collection('data').find()
    .toArray(function (err, items) {
      res.send(items)
    })
})

// updating a data by it's ID and new value
router.put('/update-todo', function (req, res) {
  db.collection('data').findOneAndUpdate(
    { _id: new mongodb.ObjectId(req.body.id) },
    { $set: { name: req.body.name, done: req.body.done } },
    function (err, object) {
      if (err) {
        console.warn(err.message);  // returns error if no matching object found
      } else {
        console.dir(object);
        res.send('Success updated!');
      }
    }
  )
})

// deleting a data by it's ID
router.delete('/delete-todo', function (req, res) {
  db.collection('data').deleteOne(
    { _id: new mongodb.ObjectId(req.body.id) },
    function () {
      res.send('Successfully deleted!')
    }
  )
})

// getting all the data about one parameter 
router.get('/todos-filter/:param_boolean', function (req, res) {
  db.collection('data').find({ done: JSON.parse(req.params.param_boolean) })
    .toArray(function (err, items) {
      res.send(items)
    })
})

module.exports = router;