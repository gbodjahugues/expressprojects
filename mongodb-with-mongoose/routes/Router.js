const express = require('express');
const routes = express.Router();
const repository = require('../repositories/TodoRepository');

// get all todo items in the db
routes.get('/', (req, res) => {
  repository.findAll().then((todos) => {
    res.json(todos);
  }).catch((error) => console.log(error));
});

// add a todo item
routes.post('/', (req, res) => {
  const { name } = req.body;
  repository.create(name).then((todo) => {
    res.json(todo);
  }).catch((error) => console.log(error));
});

// delete a todo item
routes.delete('/:id', (req, res) => {
  const { id } = req.params.id;
  repository.deleteById(id).then((ok) => {
    console.log(ok);
    console.log(`Deleted record with id: ${id}`);
    res.status(200).json(['Todo has been deleted']);
  }).catch((error) => console.log(error));
});

// update a todo item
routes.put('/:id', (req, res) => {
  const { id } = req.params.id
  const todo = { name: req.body.name, done: req.body.done };
  repository.updateById(id, todo)
    .then(res.status(200).json(['Todo has been updated']))
    .catch((error) => console.log(error));
});


// done a todo item
routes.put('/done/:id', (req, res) => {
  const { id } = req.params;
  const todo = { done: true };
  repository.doneById(id, todo)
    .then(res.status(200).json(['Todo is done']))
    .catch((error) => console.log(error));
});



// get all todo items about one parameter
routes.get('/todos-filtre/:param_boolean', (req, res) => {
  repository.filtre(req.params.param_boolean)
    .then((todos) => {res.status(200).json(todos)})
    .catch((error) => console.log(error));
});

module.exports = routes;